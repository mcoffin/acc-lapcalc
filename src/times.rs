use std::{
    borrow::Cow,
    fmt::Display,
    io::{
        self,
        ErrorKind,
    },
    str::FromStr,
};
use regex::Regex;

fn make_err<E: ?Sized>(msg: &str, e: Option<&E>, kind: Option<ErrorKind>) -> io::Error where
    E: Display,
{
    let kind = kind.unwrap_or(ErrorKind::Other);
    let msg = if let Some(e) = e {
        Cow::Owned(format!("{}: {}", msg, e))
    } else {
        Cow::Borrowed(msg)
    };
    io::Error::new(kind, msg)
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct RaceDuration(pub f64);

impl FromStr for RaceDuration {
    type Err = io::Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref TIME_PATTERN: Regex = Regex::new(r"^([0-9]+):([0-9]+)(.[0-9]+)?$").unwrap();
        }
        let time = TIME_PATTERN.captures(s)
            .map(|caps| {
                let get_cap = move |index: usize| {
                    let s = match index {
                        3 => caps.get(index).map(|s| format!("0{}", s.as_str())),
                        index => Some(caps.get(index).unwrap().as_str().to_string()),
                    };
                    s.map(|s| s.parse::<f64>().unwrap())
                };
                let minutes = get_cap(1).unwrap();
                let seconds = get_cap(2).unwrap();
                let partial_seconds = get_cap(3).unwrap_or(0.0);
                (minutes * 60.0) + seconds + partial_seconds
            })
            .or_else(|| s.parse::<f64>().ok())
            .map_or_else(|| Err(make_err::<io::Error>(&format!("Invalid time value: \"{}\"", s), None, None)), Ok)?;
        Ok(RaceDuration(time))
    }
}

impl Into<f64> for RaceDuration {
    #[inline]
    fn into(self) -> f64 {
        self.0
    }
}
