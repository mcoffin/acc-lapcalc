use log::*;
use std::{
    env,
    ffi::OsStr,
    sync::Once,
};

static INIT_LOGGING: Once = Once::new();
pub const DEFAULT_LOG_LEVEL: &'static str = "info";

fn set_env_default<K, V>(key: K, value: V) where
    K: AsRef<OsStr>,
    V: AsRef<OsStr>,
{
    let key = key.as_ref();
    if env::var_os(key).is_none() {
        env::set_var(key, value);
    }
}

pub fn init() {
    INIT_LOGGING.call_once(|| {
        set_env_default("RUST_LOG", format!("{}={}", env!("CARGO_CRATE_NAME"), DEFAULT_LOG_LEVEL));
        debug!("Initialized logging with RUST_LOG: {}", env::var("RUST_LOG").unwrap());
        env_logger::init();
    });
}
