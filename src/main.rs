extern crate clap;
extern crate log;
extern crate env_logger;
#[macro_use] extern crate lazy_static;
extern crate regex;

mod logging;
pub mod times;

use clap::{
    crate_version,
    crate_authors,
    crate_description,
};
use log::*;
use std::{
    fmt::Display,
    io::{
        self,
        BufRead,
        ErrorKind,
    },
    process,
    str::FromStr,
};
use times::RaceDuration;

#[derive(Debug, clap::Parser)]
#[command(version = crate_version!(), author = crate_authors!(), about = crate_description!())]
pub struct Config {
    #[command(subcommand)]
    subcommand: Mode,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Function {
    FuelCalc,
    StintCalc,
}

#[derive(Debug, clap::Subcommand)]
pub enum Mode {
    Cli(LaptimeArgs),
    Interactive,
    StintLength(StintLengthArgs),
    StintLengthInteractive,
}

fn parse_io<T, S: AsRef<str>>(s: S) -> io::Result<T> where
    T: FromStr,
    <T as FromStr>::Err: Display,
{
    s.as_ref().parse()
        .map_err(|e| io::Error::new(ErrorKind::Other, format!("{}", &e)))
}

impl Mode {
    fn function(&self) -> Function {
        match self {
            &Mode::Cli(_) | &Mode::Interactive => Function::FuelCalc,
            &Mode::StintLength(_) | &Mode::StintLengthInteractive => Function::StintCalc,
        }
    }
    fn laptime_config(&self) -> io::Result<LaptimeArgs> {
        use std::borrow::Cow;
        match self {
            &Mode::Cli(v) => Ok(v),
            &Mode::Interactive => {
                let stdin = io::stdin();
                let stdin = stdin.lock();
                let mut lines = stdin.lines();
                let mut next_line = move || {
                    lines.next()
                        .and_then(|v| v.ok())
                        .map_or_else(|| Err(io::Error::new(ErrorKind::Other, "No value entered")), Ok)
                };
                println!("lap time?");
                let lap_time: RaceDuration = next_line()
                    .and_then(|s| parse_io(s))?;
                println!("race length?");
                let race_length: RaceDuration = next_line()
                    .and_then(|s| parse_io(s))?;
                println!("fuel per lap?");
                let fuel_per_lap: f64 = next_line()
                    .and_then(|s| parse_io(s))?;
                println!("extra laps? (default: 0)");
                let extra_laps: f32 = next_line()
                    .map(|s| if s.len() > 0 {
                        Cow::Owned(s)
                    } else {
                        Cow::Borrowed("0")
                    })
                    .and_then(|s| parse_io(s))?;
                println!("# of stops?");
                let stops: u8 = {
                    let s = next_line()?;
                    if s.len() <= 0 {
                        0
                    } else {
                        parse_io(s)?
                    }
                };
                let pit_delta: Option<RaceDuration> = if stops > 0 {
                    println!("pit delta?");
                    next_line()
                        .and_then(|s| parse_io(s))
                        .map(Some)?
                } else {
                    None
                };
                Ok(LaptimeArgs::new(lap_time, race_length, fuel_per_lap, pit_delta.map(|delta| (stops, delta)), Some(extra_laps)))
            },
            _ => {
                let msg = format!("Invalid mode for laptime_config: {:?}", self);
                Err(io::Error::new(io::ErrorKind::Other, msg))
            },
        }
    }
    fn stint_length_config(&self) -> io::Result<StintLengthArgs> {
        match self {
            &Mode::StintLength(v) => Ok(v),
            &Mode::StintLengthInteractive => {
                let stdin = io::stdin();
                let stdin = stdin.lock();
                let mut lines = stdin.lines();
                let mut next_line = move || {
                    lines.next()
                        .and_then(Result::ok)
                        .map_or_else(|| Err(io::Error::new(ErrorKind::Other, "No value entered")), Ok)
                };
                println!("lap time?");
                let lap_time: RaceDuration = next_line()
                    .and_then(|s| parse_io(s))?;
                println!("fuel per lap?");
                let fuel_per_lap: f64 = next_line()
                    .and_then(|s| parse_io(s))?;
                println!("fuel in tank?");
                let fuel_in_tank: f64 = next_line()
                    .and_then(|s| parse_io(s))?;
                Ok(StintLengthArgs {
                    lap_time: lap_time,
                    fuel_per_lap: fuel_per_lap,
                    fuel_in_tank: fuel_in_tank,
                })
            },
            _ => {
                let msg = format!("Invalid mode for stint_length_config: {:?}", self);
                Err(io::Error::new(io::ErrorKind::Other, msg))
            },
        }
    }
}

#[derive(Debug, Clone, Copy, clap::Args)]
pub struct StintLengthArgs {
    #[clap(long)]
    lap_time: RaceDuration,
    #[clap(long)]
    pub fuel_per_lap: f64,
    #[clap(long)]
    pub fuel_in_tank: f64,
}

impl StintLengthArgs {
    #[inline(always)]
    pub fn lap_time(&self) -> f64 {
        self.lap_time.into()
    }

    #[inline(always)]
    pub fn available_fuel(&self) -> f64 {
        self.fuel_in_tank - 0.7
    }

    pub fn estimated_laps(&self) -> (f64, f64) {
        let laps = self.available_fuel() / self.fuel_per_lap;
        (laps.floor(), laps)
    }

    pub fn remaining_fuel(&self, laps: f64) -> f64 {
        self.fuel_in_tank - (laps * self.fuel_per_lap)
    }

    // pub fn estimated_time(&self) -> f64 {
    //     let (laps, _real_laps) = self.estimated_laps();
    //     laps * self.lap_time()
    // }
}

#[derive(Debug, Clone, Copy, clap::Args)]
pub struct LaptimeArgs {
    #[clap(long)]
    lap_time: RaceDuration,
    #[clap(long)]
    race_length: RaceDuration,
    #[clap(long, short)]
    fuel_per_lap: f64,
    #[clap(long, default_value = "0")]
    stops: u8,
    #[clap(long)]
    pit_delta: Option<RaceDuration>,
    #[clap(long)]
    extra_laps: Option<f32>,
}

impl LaptimeArgs {
    #[inline(always)]
    pub fn new(lap_time: RaceDuration, race_length: RaceDuration, fuel_per_lap: f64, stop_data: Option<(u8, RaceDuration)>, extra_laps: Option<f32>) -> Self {
        let stops = stop_data.map(|(v, _)| v).unwrap_or(0);
        LaptimeArgs {
            lap_time: lap_time,
            race_length: race_length,
            fuel_per_lap: fuel_per_lap,
            stops: stops,
            pit_delta: stop_data.map(|(_, v)| v),
            extra_laps: extra_laps,
        }
    }
    #[inline(always)]
    pub fn lap_time(&self) -> f64 {
        self.lap_time.into()
    }
    #[inline(always)]
    pub fn race_length(&self) -> f64 {
        match self.pit_delta.map(|delta| (self.stops, delta)) {
            Some((stops, delta)) if stops > 0 => {
                let delta: f64 = delta.into();
                let length: f64 = self.race_length.into();
                length - (stops as f64 * delta)
            },
            _ => self.race_length.into(),
        }
    }

    pub fn exact_estimated_laps(&self) -> f64 {
        self.race_length() as f64 / self.lap_time()
    }

    pub fn estimated_laps(&self) -> f64 {
        self.exact_estimated_laps().ceil()
    }

    #[inline(always)]
    pub fn extra_laps(&self) -> f32 {
        self.extra_laps.unwrap_or(0f32)
    }

    pub fn estimated_fuel_laps(&self) -> f64 {
        self.estimated_laps() + self.extra_laps() as f64
    }

    pub fn estimated_fuel(&self) -> f64 {
        self.estimated_fuel_laps() * self.fuel_per_lap
    }

    pub fn fuel_per_minute(&self) -> f64 {
        (self.fuel_per_lap / self.lap_time()) * 60.0
    }

    #[inline(always)]
    pub fn stints(&self) -> u8 {
        self.stops + 1
    }

    pub fn estimated_stint_fuel(&self) -> f64 {
        if self.stops == 0 {
            self.estimated_fuel()
        } else {
            self.estimated_fuel() / self.stints() as f64
        }
    }
}
fn real_main(config: &Config) -> io::Result<()> {
    match config.subcommand.function() {
        Function::FuelCalc => {
            let config = config.subcommand.laptime_config()?;
            println!("laps: {} ({})\nfuel: {}", config.estimated_laps(), config.exact_estimated_laps(), config.estimated_fuel());
            if config.stops > 0 {
                println!("stint fuel: {}", config.estimated_stint_fuel());
            }
            println!("fuel per min: {}", config.fuel_per_minute());
            Ok(())
        },
        Function::StintCalc => {
            let config = config.subcommand.stint_length_config()?;
            let (whole_laps, partial_laps) = config.estimated_laps();
            println!("laps: {} ({})", whole_laps, partial_laps);
            println!("remaining at end: {}", config.remaining_fuel(whole_laps));
            Ok(())
        },
    }
}

fn main() {
    use clap::Parser;
    logging::init();
    let config = Config::parse();
    debug!("config: {:?}", &config);
    if let Err(e) = real_main(&config) {
        error!("{}", &e);
        process::exit(1);
    }
}
